#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''

'''

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)

def setup_route(app):
    import api
    import web

    app = api.setup(app)
    app = web.setup(app)

    return app

if __name__ == '__main__':
    app = setup_route(app)

    app.debug = True
    app.run()

