#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
數據庫表設計
'''

import json
from typing import Optional
from sqlalchemy import Column, Integer, Unicode, Text, ForeignKey, Table
from sqlalchemy.orm import relationship
from app import db

# association table
article_tag = Table('article_tag', db.metadata,
                    Column('article_id', ForeignKey('article.id'), primary_key=True),
                    Column('tag_id', ForeignKey('tag.id'), primary_key=True)
                   )

class Article(db.Model):
    __tablename__ = 'article'

    _id = Column('id', Integer, primary_key=True, autoincrement=True, nullable=False)
    content = Column(Text)
    tags = relationship('Tag', secondary=article_tag, back_populates='articles')

    def __init__(self, content, tags: Optional[list]=None) -> 'Article':
        self.content = content
        if tags:
            for tag in tags:
                self.tags.append(Tag(tag))

    @staticmethod
    def from_json(jdict) -> 'Article':
        content = jdict['content']
        article = Article(content)
        try:
            tags = jdict['tags']
            for tag in tags:
                article.tags.append(Tag(tag))
        except KeyError:
            tags = None
        return article

    def __json__(self) -> dict:
        return {
            'content': self.content,
            'tags': [tag.tag for tag in self.tags],
            }

class Tag(db.Model):
    __tablename__ = 'tag'

    _id = Column('id', Integer, primary_key=True, autoincrement=True, nullable=False)
    tag = Column('tag', Unicode(40))
    articles = relationship('Article',
                            secondary=article_tag,
                            back_populates='tags',
                            lazy='dynamic')

    def __init__(self, tag):
        self.tag = tag

