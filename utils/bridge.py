#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''

'''

from typing import Union
from database import Article
from app import db

class InfoNotFound(Exception):pass

def list_info() -> list:
    article = Article.query.all()
    ids = [a._id for a in article]
    return ids

def get_info(_id: int) -> Article:
    article = Article.query.filter_by(_id=_id).all()
    if len(article) != 1:
        raise InfoNotFound()
    return article[0]

def add_info(article: Article) -> dict:
    db.session.add(article)
    db.session.commit()
    return {'id': article._id}

def edit_info(_id: int, new: Article) -> None:
    old = get_info(_id)
    new._id = old._id
    db.session.add(new)
    db.session.commit()

