#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''

'''

from flask import render_template, request, json, abort, url_for, redirect
from database import Article
import utils

def extract_form(request) -> Article:
    try:
        content = request.form['content']
    except KeyError:
        abort(400)
    try:
        r_tags = request.form['tags']
        tags = [t.strip(' ') for t in r_tags.split(',')]
    except KeyError:
        tags = None
    return Article(content, tags)

def setup(app):
    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/info/', methods=['GET', 'POST'])
    def info_index():
        if request.method == 'GET':
            res = utils.list_info()
            return render_template('info_index.html', list=res)
        if request.method == 'POST':
            res = utils.add_info(extract_form(request))
            return 'success'

    @app.route('/info/<int:_id>', methods=['GET', 'PUT'])
    def info(_id: int):
        if request.method == 'GET':
            res = utils.get_info(_id)
            return render_template('info.html', info=res)
        if request.method == 'PUT':
            utils.edit_info(_id, extract_form(request))
            return ''

    @app.route('/add_info', methods=['GET', 'POST'])
    def add_info():
        if request.method == 'GET':
            return render_template('add_info.html')
        if request.method == 'POST':
            res = utils.add_info(extract_form(request))
            return redirect(url_for('info_index'))

    return app
