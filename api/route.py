#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''

'''

from flask import render_template, request, json, abort
from database import Article
import utils

def extract_json(request) -> Article:
    try:
        article = Article.from_json(request.get_json())
    except KeyError:
        abort(400)
    return article

def setup(app):
    @app.route('/api/info/', methods=['GET', 'POST'])
    def api__info_list():
        if request.method == 'GET':
            return json.jsonify(utils.list_info())
        if request.method == 'POST':
            return json.jsonify(utils.add_info(extract_json(request)))

    @app.route('/api/info/<int:_id>', methods=['GET', 'PUT'])
    def api__info(_id: int):
        try:
            if request.method == 'GET':
                return json.jsonify(utils.get_info(_id).__json__())
            if request.method == 'PUT':
                utils.edit_info(_id, extract_json(request))
                return ''
        except utils.InfoNotFound:
            return '', 404

    return app

