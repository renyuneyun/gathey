Gathey
========

依賴
------
* flask
* SQLAlchemy
* Flask-SQLAlchemy

運行
-------
1. 安裝好依賴庫（見上），建議使用virtualenv
2. 配置環境，並將程序目錄加入PYTHON_PATH（如使用virtualenv可直接source setup_env.sh）
3. 執行python3 app.py

結構
------
* app.py: 入口
* database: 數據庫相關（模型等），使用SQLAlchemy和Flask-SQLAlchemy
* api: API的邏輯、路由
* web: 網頁端的邏輯、路由
* templates: 網頁模板。在web/中使用
* utils: 工具函數

